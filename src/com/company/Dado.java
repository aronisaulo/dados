package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Dado
{
    private int face;
    private int numeroSorteado;

    public Dado() {
        this.informarFaces();
        this.carregarValores();
    }

    public int getNumeroSorteado() {
        return numeroSorteado;
    }

    public void setNumeroSorteado(int numeroSorteado) {
        this.numeroSorteado = numeroSorteado;
    }

    public ArrayList<Integer> getValores() {
        return valores;
    }

    public void setValores(ArrayList<Integer> valores) {
        this.valores = valores;
    }

    private ArrayList<Integer> valores = new ArrayList<Integer>();

    public void informarFaces() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informar numero de Faces :");
        this.face = scanner.nextInt();
        this.carregarValores();
    }

    public void carregarValores () {

        for (int v = 1 ; v<=this.face ; v++){
            this.valores.add(v);
        }
    }
    public int sortear () {
        Random random = new Random();
        numeroSorteado =   valores.get((Integer) random.nextInt(this.face));
        System.out.println( "Numero Sorteado : "+numeroSorteado);
        return numeroSorteado;
    }
    public void acumularNumero() {
        ArrayList<Integer> acumulado = new ArrayList<Integer>();
        for (int a=0 ; a<3 ; a++) {
            acumulado.add(this.sortear());
        }
        int somar =0;
        for (Integer numero : acumulado){
            somar += numero;

        }
        System.out.println("Soma:"+somar);
    }
     public void processar() {
        this.acumularNumero();
     }
}
